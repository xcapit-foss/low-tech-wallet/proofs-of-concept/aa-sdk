import { BigNumberish } from "@ethersproject/bignumber";
import { BytesLike } from "@ethersproject/bytes";
import { EOASigner, IPresetBuilderOpts, Presets, UserOperationBuilder } from "userop";
import { SimpleAccount } from "userop/dist/preset/builder";

export default class CustomBuilder extends UserOperationBuilder {

	constructor(private _simpleAccount: SimpleAccount) {
		super();
	}

	public static async init(signer: EOASigner, rpcUrl: string, opts?: IPresetBuilderOpts): Promise<CustomBuilder> {
		return new CustomBuilder(await Presets.Builder.SimpleAccount.init(signer, rpcUrl, opts));
	}

	public execute(to: string, value: BigNumberish, data: BytesLike) {
		return this._simpleAccount.execute(to, value, data);
	}

	public executeBatch(to: Array<string>, data: Array<BytesLike>) {
		return this._simpleAccount.executeBatch(to, data);
	}
}
