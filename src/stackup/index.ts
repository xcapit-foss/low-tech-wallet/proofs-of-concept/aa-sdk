
import { ethers } from 'ethers';
import { Presets, Client, IPresetBuilderOpts } from 'userop';
import * as erc20Data from './erc20.json';
import { env } from 'process';
import dotenv from 'dotenv';

dotenv.config();

const { S_PAYMASTER_URL, S_PRIVATE_KEY, S_RPC_URL } = env;

const to = '0xF7bdBd1e3dE6D9B3597255E2E4A0b66CDCCCC8A8';
const link = '0x326C977E6efc84E512bB9C30f76E30c160eD06FB';
// const value = '1000000000000000000';
const value = '0';

function paymasterContext() {
  return { type: 'payg' };
}

function paymasterMiddleware() {
  return Presets.Middleware.verifyingPaymaster(
    S_PAYMASTER_URL!,
    paymasterContext()
  );
}

async function main() {
  const signer = new ethers.Wallet(S_PRIVATE_KEY!);

  const options: IPresetBuilderOpts = {
    paymasterMiddleware: paymasterMiddleware(),
  };

  let builder = await Presets.Builder.SimpleAccount.init(signer, S_RPC_URL!, options);
  // let builder = await CustomBuilder.init(signer, S_RPC_URL!, options);

  const address = builder.getSender();

  console.log(`Account address: ${address}`);

  const client = await Client.init(S_RPC_URL!, options);
  console.log(`Entry Point: ${client.entryPoint.address}`);


  const erc20 = new ethers.Contract(link, erc20Data.abi, new ethers.JsonRpcProvider(S_RPC_URL!));
  const amount = ethers.parseUnits(value, 18);

  // Encode the calls
  const callTo = [
    link,
    // link
  ];
  const callData = [
    erc20.interface.encodeFunctionData("approve", [to, 100000000]),
    // erc20.interface.encodeFunctionData("transfer", [to, '1']),
  ];

  console.log('Sending user operation....');
  const res = await client.sendUserOperation(
    builder.executeBatch(callTo, callData),
    {
      // dryRun: true,
      onBuild: (op) => console.log('Signed UO:', op),
    }
  );

  console.log('User Operation Hash:', res.userOpHash);
  const ev = await res.wait();
  console.log(`Transaction hash: ${ev?.transactionHash ?? null}`);
  console.log(`View here: https://jiffyscan.xyz/userOpHash/${res.userOpHash}`);
}

main().catch((err) => console.log(`Error: ${err}`));
