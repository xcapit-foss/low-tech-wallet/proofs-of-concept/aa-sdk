import { Address } from '@alchemy/aa-core';
import { encodeFunctionData } from 'viem';
import { lightAccountClient } from './light-account-client';

(async () => {
  const client = await lightAccountClient();

  console.log('Smart Account Address: ', client.getAddress());

  const erc20TransferAbi = [
    {
      inputs: [
        {
          internalType: 'address',
          name: 'to',
          type: 'address',
        },
        {
          internalType: 'uint256',
          name: 'value',
          type: 'uint256',
        },
      ],
      name: 'transfer',
      outputs: [
        {
          internalType: 'bool',
          name: '',
          type: 'bool',
        },
      ],
      stateMutability: 'nonpayable',
      type: 'function',
    },
  ];

  const transferData = encodeFunctionData({
    abi: erc20TransferAbi,
    functionName: 'transfer',
    args: ['0xF7bdBd1e3dE6D9B3597255E2E4A0b66CDCCCC8A8', 5e18],
  });

  const linkAddress = '0x326C977E6efc84E512bB9C30f76E30c160eD06FB' as Address;

  const { hash: uoHash } = await client.sendUserOperation({
    uo: {
      target: linkAddress,
      data: transferData,
    },
  });

  console.log('UserOperation Hash: ', uoHash);

  const txHash = await client.waitForUserOperationTransaction({
    hash: uoHash,
  });

  console.log('Transaction Hash: ', txHash);
})();
