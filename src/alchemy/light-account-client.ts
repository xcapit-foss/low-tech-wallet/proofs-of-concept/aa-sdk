import { createLightAccountAlchemyClient } from '@alchemy/aa-alchemy';
import { LocalAccountSigner, polygonMumbai, type Hex } from '@alchemy/aa-core';
import { env } from 'process';
import dotenv from 'dotenv';

dotenv.config();

const { PRIVATE_KEY, API_KEY, POLICY_ID } = env;
const chain = polygonMumbai;

const signer = LocalAccountSigner.privateKeyToAccountSigner(PRIVATE_KEY as Hex);

export const lightAccountClient = async () =>
  await createLightAccountAlchemyClient({
    apiKey: API_KEY,
    chain,
    signer,
    gasManagerConfig: {
      policyId: POLICY_ID!,
    },
  });
