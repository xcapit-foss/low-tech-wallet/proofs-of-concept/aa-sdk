import { createSmartAccountClient, polygonMumbai } from "@alchemy/aa-core";
import { Presets} from 'userop';
import { http } from "viem";
import { env } from 'process';
import dotenv from 'dotenv';

dotenv.config();

const { RPC_URL } = env;
const paymasterUrl = 'https://api.stackup.sh/v1/paymaster/a738e4401206f7707d678604ca96ed37349fc322f00b1617cb3cdcf6f4bf5e99';
const chain = polygonMumbai;
const client = createSmartAccountClient({
  chain,
  transport: http(RPC_URL),
  paymasterAndData: {
    paymasterAndData: async (userop, opts) => {
        // call your paymaster here to sponsor the userop
        const paymasterContext = { type: "payg" };
        const paymasterMiddleware = Presets.Middleware.verifyingPaymaster(
          paymasterUrl,
          paymasterContext
        );
        // leverage the `opts` field to apply any overrides
        opts = paymasterUrl === "" ? {} : {
          paymasterMiddleware: paymasterMiddleware,
        }
        
        // Simulate signing a transaction with a private key (replace with your actual signing logic)
        // const privateKey = "YOUR_PRIVATE_KEY_HERE"; // Replace with your private key
        // const provider = new ethers.providers.JsonRpcProvider("RPC_URL"); // Replace with your RPC URL
        // const wallet = new ethers.Wallet(privateKey, provider);
        // const tx = await signTransaction(wallet, userop.transaction); // Sign the transaction
        // const signedTx = await provider.sendTransaction(tx); // Send the signed transaction to the network
        // return signedTx.hash; // Return the transaction hash
        return {
            ...userop,
            paymasterAndData: "0xresponsefromprovider"
        }
    },
    dummyPaymasterAndData: () => "0xnonrevertingpaymasterandata",
  },
});