import {
  createLightAccount,
  lightAccountClientActions,
} from '@alchemy/aa-accounts';
import {
  LocalAccountSigner,
  SmartAccountSigner,
  createSmartAccountClient,
  polygonMumbai,
  Address,
} from '@alchemy/aa-core';
import { Hex, http, encodeFunctionData } from 'viem';
import { env } from 'process';
import dotenv from 'dotenv';
import { lightAccountClient } from './light-account-client';

dotenv.config();

const { RPC_URL, PRIVATE_KEY } = env;

export const chain = polygonMumbai;
export const signer: SmartAccountSigner =
  LocalAccountSigner.privateKeyToAccountSigner(PRIVATE_KEY as Hex);
export const rpcTransport = http(RPC_URL);

export const smartAccountClient = async () =>
  createSmartAccountClient({
    gasEstimator: async (struct) => ({
      ...struct,
      callGasLimit: BigInt(12100),
      preVerificationGas: BigInt(49892),
      verificationGasLimit: BigInt(12100),
    }),    
    transport: rpcTransport,
    chain,
    account: (await lightAccountClient()).account, 
}).extend(lightAccountClientActions);

(async () => {
  const client = await smartAccountClient();

  console.log('Smart Account Address: ', client.getAddress());

  const erc20TransferAbi = [
    {
      inputs: [
        {
          internalType: 'address',
          name: 'to',
          type: 'address',
        },
        {
          internalType: 'uint256',
          name: 'value',
          type: 'uint256',
        },
      ],
      name: 'transfer',
      outputs: [
        {
          internalType: 'bool',
          name: '',
          type: 'bool',
        },
      ],
      stateMutability: 'nonpayable',
      type: 'function',
    },
  ];

  const transferData = encodeFunctionData({
    abi: erc20TransferAbi,
    functionName: 'transfer',
    args: ['0xF7bdBd1e3dE6D9B3597255E2E4A0b66CDCCCC8A8', 5e18],
  });

  const linkAddress = '0x326C977E6efc84E512bB9C30f76E30c160eD06FB' as Address;

  const { hash: uoHash } = await client.sendUserOperation({
    uo: {
      target: linkAddress,
      data: transferData,
    },
  });

  console.log('UserOperation Hash: ', uoHash);

  const txHash = await client.waitForUserOperationTransaction({
    hash: uoHash,
  });

  console.log('Transaction Hash: ', txHash);
})();
// export const smartAccountClient = async () =>
//   createSmartAccountClient({
//     transport: rpcTransport,
//     chain,
//     account: await createLightAccount({
//       transport: rpcTransport,
//       chain,
//       signer,
//     }),
//   }).extend(lightAccountClientActions);
